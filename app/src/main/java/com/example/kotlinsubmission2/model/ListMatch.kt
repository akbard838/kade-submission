package com.example.kotlinsubmission2.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ListMatch(
    var idDB : Long?,
    var id: String?,
    var date: String?,
    var homeName: String?,
    var homeScore: String?,
    var homeBadge: String?,
    var strHomeGoalDetails: String?,
    var strHomeLineupDefense: String?,
    var strHomeLineupForward: String?,
    var strHomeLineupGoalkeeper: String?,
    var strHomeLineupMidfield: String?,
    var strHomeLineupSubstitutes: String?,
    var awayName: String?,
    var awayScore: String?,
    var awayBadge: String?,
    var strAwayGoalDetails: String?,
    var strAwayLineupDefense: String?,
    var strAwayLineupForward: String?,
    var strAwayLineupGoalkeeper: String?,
    var strAwayLineupMidfield: String?,
    var strAwayLineupSubstitutes: String?
) : Parcelable {

    companion object {
        const val TABLE_FAVORITE: String = "TABLE_FAVORITE"
        const val ID: String = "ID_"
        const val MATCH_ID: String = "MATCH_ID"
        const val MATCH_DATE: String = "MATCH_DATE"
        const val HOME_NAME: String = "HOME_NAME"
        const val HOME_SCORE: String = "HOME_SCORE"
        const val HOME_BADGE: String = "HOME_BADGE"
        const val HOME_GOALS: String = "HOME_GOALS"
        const val HOME_DEFENSE: String = "HOME_DEFENSE"
        const val HOME_FORWARD: String = "HOME_FORWARD"
        const val HOME_GOALKEEPER: String = "HOME_GOALKEEPER"
        const val HOME_MIDFIELD: String = "HOME_MIDFIELD"
        const val HOME_SUBS: String = "HOME_SUBS"
        const val AWAY_NAME: String = "AWAY_NAME"
        const val AWAY_SCORE: String = "AWAY_SCORE"
        const val AWAY_BADGE: String = "AWAY_BADGE"
        const val AWAY_GOALS: String = "AWAY_GOALS"
        const val AWAY_DEFENSE: String = "AWAY_DEFENSE"
        const val AWAY_FORWARD: String = "AWAY_FORWARD"
        const val AWAY_GOALKEEPER: String = "AWAY_GOALKEEPER"
        const val AWAY_MIDFIELD: String = "AWAY_MIDFIELD"
        const val AWAY_SUBS: String = "AWAY_SUBS"
    }

}


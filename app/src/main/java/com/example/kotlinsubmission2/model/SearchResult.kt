package com.example.kotlinsubmission2.model

import com.google.gson.annotations.SerializedName

data class SearchResult(
    @SerializedName("event") var events: List<Event>
)
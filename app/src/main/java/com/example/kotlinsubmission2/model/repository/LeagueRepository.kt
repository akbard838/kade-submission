package com.example.kotlinsubmission2.model.repository

import com.example.kotlinsubmission2.model.Leagues
import io.reactivex.Flowable

interface LeagueRepository {

    fun getLeagues(s : String) : Flowable<Leagues>

}
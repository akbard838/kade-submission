package com.example.kotlinsubmission2.model

import com.google.gson.annotations.SerializedName

data class Leagues(
    @SerializedName("countrys")
    var countrys: List<League>)
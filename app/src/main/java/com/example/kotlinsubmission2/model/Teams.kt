package com.example.kotlinsubmission2.model

import com.google.gson.annotations.SerializedName

data class Teams(
    @SerializedName("teams") var teams: List<Team>)

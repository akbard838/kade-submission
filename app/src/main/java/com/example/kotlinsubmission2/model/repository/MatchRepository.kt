package com.example.kotlinsubmission2.model.repository

import com.example.kotlinsubmission2.model.FootballMatch
import com.example.kotlinsubmission2.model.SearchResult
import com.example.kotlinsubmission2.model.Teams
import io.reactivex.Flowable

interface MatchRepository {

    fun getPreviousMatch(id : String) : Flowable<FootballMatch>

    fun getNextMatch(id : String) : Flowable<FootballMatch>

    fun getEventById(id: String) : Flowable<FootballMatch>

    fun searchMatches(query: String?) : Flowable<SearchResult>

    fun getTeamsDetail(id : String = "0") : Flowable<Teams>
}
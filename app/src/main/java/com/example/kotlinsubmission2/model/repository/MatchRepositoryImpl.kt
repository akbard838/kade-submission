package com.example.kotlinsubmission2.model.repository

import com.example.kotlinsubmission2.source.api.FootballRest
import com.example.kotlinsubmission2.model.FootballMatch
import com.example.kotlinsubmission2.model.Teams
import io.reactivex.Flowable

class MatchRepositoryImpl(private val footballRest: FootballRest) : MatchRepository {

    override fun searchMatches(query: String?) = footballRest.searchMatches(query)

    override fun getEventById(id: String): Flowable<FootballMatch> = footballRest.getEventById(id)

    override fun getNextMatch(id: String): Flowable<FootballMatch> = footballRest.getNextMatch(id)

    override fun getPreviousMatch(id: String): Flowable<FootballMatch> = footballRest.getPreviousMatch(id)

    override fun getTeamsDetail(id: String): Flowable<Teams> = footballRest.getTeam(id)
}
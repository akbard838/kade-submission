package com.example.kotlinsubmission2.model.repository

import com.example.kotlinsubmission2.model.Leagues
import com.example.kotlinsubmission2.source.api.FootballRest
import io.reactivex.Flowable

class LeagueRepositoryImpl(private val footballRest: FootballRest) : LeagueRepository {

    override fun getLeagues(s: String): Flowable<Leagues> = footballRest.getLeague(s)

}
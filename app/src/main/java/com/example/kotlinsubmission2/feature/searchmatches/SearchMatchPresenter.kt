package com.example.kotlinsubmission2.feature.searchmatches

import com.example.kotlinsubmission2.model.repository.MatchRepositoryImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SearchMatchPresenter(val mView: SearchMatchContract.View,
                           val matchRepositoryImpl: MatchRepositoryImpl): SearchMatchContract.Presenter {

    override fun getTeamsBadgeAway(id: String, position: Int) {
        compositeDisposable.add(matchRepositoryImpl.getTeamsDetail(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe{
                mView.displayTeamBadgeAway(it.teams[0], position)
            })
    }

    override fun getTeamsBadgeHome(id: String, position: Int) {
        compositeDisposable.add(matchRepositoryImpl.getTeamsDetail(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe{
                mView.displayTeamBadgeHome(it.teams[0], position)
            })
    }

    private val compositeDisposable = CompositeDisposable()

    override fun searchMatch(query: String?) {
        mView.showLoading()
        compositeDisposable.add(matchRepositoryImpl.searchMatches(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({
                    mView.hideLoading()
                    mView.displayMatch(it.events)
                }, {
                    mView.showEmpty()
                }))
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }


}
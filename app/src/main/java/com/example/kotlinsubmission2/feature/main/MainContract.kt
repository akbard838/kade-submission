package com.example.kotlinsubmission2.feature.main

import com.example.kotlinsubmission2.model.League

interface MainContract {

    interface View{
        fun hideLoading()
        fun showLoading()
        fun displayLeague(leagueList:List<League>)
    }

    interface Presenter{
        fun getLeagueData()

    }

}
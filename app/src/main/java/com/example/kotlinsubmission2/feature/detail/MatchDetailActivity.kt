package com.example.kotlinsubmission2.feature.detail

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.kotlinsubmission2.R
import com.example.kotlinsubmission2.model.ListMatch
import com.example.kotlinsubmission2.source.db.database
import com.example.kotlinsubmission2.util.setImageUrl
import kotlinx.android.synthetic.main.activity_match_detail.*
import kotlinx.android.synthetic.main.layout_item_match.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.select
import org.jetbrains.anko.startActivity

class MatchDetailActivity : AppCompatActivity() {

    private var match : ListMatch? = null
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    companion object {
        fun start(
            context: Context,
            match : ListMatch?) {
            context.startActivity<MatchDetailActivity>(
                "match" to match
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match_detail)

        match = intent?.getParcelableExtra("match")

        initData(match)

        toolbarMatchDetail.title = " "
        setSupportActionBar(toolbarMatchDetail)

        favoriteState()
    }

    private fun initData(match: ListMatch?){
        tvDate.text = match?.date

        imgHome.setImageUrl(this, match?.homeBadge ?: " ", pbHome)
        tvScoreHome.text = match?.homeScore
        tvNameHome.text = match?.homeName
        tvHomeGoal.text = match?.strHomeGoalDetails ?: "-"
        tvHomeDefense.text = match?.strHomeLineupDefense ?: "-"
        tvHomeForward.text = match?.strHomeLineupForward ?: "-"
        tvHomeGoalKeeper.text = match?.strHomeLineupGoalkeeper ?: "-"
        tvHomeMidfield.text = match?.strHomeLineupMidfield ?: "-"
        tvHomeSubs.text = match?.strHomeLineupSubstitutes ?: "-"

        imgAway.setImageUrl(this, match?.awayBadge ?: " ", pbAway)
        tvScoreAway.text = match?.awayScore
        tvNameAway.text = match?.awayName
        tvAwayGoal.text = match?.strAwayGoalDetails ?: "-"
        tvAwayDefense.text = match?.strAwayLineupDefense ?: "-"
        tvAwayForward.text = match?.strAwayLineupForward ?: "-"
        tvAwayGoalKeeper.text = match?.strAwayLineupGoalkeeper ?: "-"
        tvAwayMidfield.text = match?.strAwayLineupMidfield ?: "-"
        tvAwaySubs.text = match?.strAwayLineupSubstitutes ?: "-"
    }

    private fun addToFavorite(){
        try {
            database.use {
                insert(
                    ListMatch.TABLE_FAVORITE,
                    ListMatch.MATCH_ID to match?.id,
                    ListMatch.MATCH_DATE to match?.date,
                    ListMatch.HOME_NAME to match?.homeName,
                    ListMatch.HOME_SCORE to match?.homeScore,
                    ListMatch.HOME_BADGE to match?.homeBadge,
                    ListMatch.HOME_GOALS to match?.strHomeGoalDetails,
                    ListMatch.HOME_DEFENSE to match?.strHomeLineupDefense,
                    ListMatch.HOME_FORWARD to match?.strHomeLineupForward,
                    ListMatch.HOME_GOALKEEPER to match?.strHomeLineupGoalkeeper,
                    ListMatch.HOME_MIDFIELD to match?.strHomeLineupMidfield,
                    ListMatch.HOME_SUBS to match?.strHomeLineupSubstitutes,
                    ListMatch.AWAY_NAME to match?.awayName,
                    ListMatch.AWAY_SCORE to match?.awayScore,
                    ListMatch.AWAY_BADGE to match?.awayBadge,
                    ListMatch.AWAY_GOALS to match?.strAwayGoalDetails,
                    ListMatch.AWAY_DEFENSE to match?.strAwayLineupDefense,
                    ListMatch.AWAY_FORWARD to match?.strAwayLineupForward,
                    ListMatch.AWAY_GOALKEEPER to match?.strAwayLineupGoalkeeper,
                    ListMatch.AWAY_MIDFIELD to match?.strAwayLineupMidfield,
                    ListMatch.AWAY_SUBS to match?.strAwayLineupSubstitutes)
            }
            Toast.makeText(this, getString(R.string.message_add_success), Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException){
            Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
        }
    }

    private fun removeFromFavorite(){
        try {
            database.use {
                delete(ListMatch.TABLE_FAVORITE, "(MATCH_ID = {id})",
                    "id" to match?.id!!
                )
            }
            Toast.makeText(this, getString(R.string.message_remove_success), Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException){
            Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorited)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite)
    }

    private fun favoriteState(){
        database.use {
            val result = select(ListMatch.TABLE_FAVORITE)
                .whereArgs("(MATCH_ID = {id})",
                    "id" to match?.id!!)
            val favorite = result.parseList(classParser<ListMatch>())
            if (favorite.isNotEmpty()) isFavorite = true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_favorite, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.actionFavorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()

                isFavorite = !isFavorite
                setFavorite()

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

}
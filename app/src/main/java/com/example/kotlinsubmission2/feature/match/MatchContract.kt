package com.example.kotlinsubmission2.feature.match

import com.example.kotlinsubmission2.model.Event
import com.example.kotlinsubmission2.model.Team

interface MatchContract {
    interface View{
        fun hideLoading()
        fun showLoading()
        fun showEmptyData()
        fun displayMatch (events:List<Event>)
        fun displayTeamBadgeHome(team: Team, position: Int = 0)
        fun displayTeamBadgeAway(team: Team, position: Int = 0)
    }

    interface Presenter{
        fun getMatchData(idLeague: String = "4328")
        fun onDestroyPresenter()
        fun getTeamsBadgeAway(id:String, position: Int = 0)
        fun getTeamsBadgeHome(id:String, position: Int = 0)
    }
}
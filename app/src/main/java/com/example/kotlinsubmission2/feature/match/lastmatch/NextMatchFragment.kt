package com.example.kotlinsubmission2.feature.match.lastmatch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlinsubmission2.R
import com.example.kotlinsubmission2.adapter.MatchAdapter
import com.example.kotlinsubmission2.source.api.FootballApiService
import com.example.kotlinsubmission2.source.api.FootballRest
import com.example.kotlinsubmission2.feature.match.MatchContract
import com.example.kotlinsubmission2.model.Event
import com.example.kotlinsubmission2.model.League
import com.example.kotlinsubmission2.model.ListMatch
import com.example.kotlinsubmission2.model.Team
import com.example.kotlinsubmission2.model.repository.MatchRepositoryImpl
import com.example.kotlinsubmission2.util.AppSchedulerProvider
import com.example.kotlinsubmission2.util.gone
import com.example.kotlinsubmission2.util.visible
import kotlinx.android.synthetic.main.fragment_match.*

class NextMatchFragment : Fragment(), MatchContract.View {

    lateinit var nextMatchPresenter: NextMatchPresenter
    private var league : League? = null
    private val match: MutableList<ListMatch> = mutableListOf()
    private var matchAdapter : MatchAdapter? = null
    private var isLoad : Boolean = false
    private val scheduler = AppSchedulerProvider()

    companion object {
        fun newInstance(league : League?): NextMatchFragment {
            val fragment = NextMatchFragment()
            val bundle = Bundle()
            bundle.putParcelable("league", league)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val service = FootballApiService.getClient().create(FootballRest::class.java)
        val request = MatchRepositoryImpl(service)

        arguments?.let {
            league = it.getParcelable("league")
        }

        nextMatchPresenter =
            NextMatchPresenter(
                this,
                request,
                scheduler)

        league?.let {
            nextMatchPresenter.getMatchData(it.idLeague ?: "error")
        }

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_match, container, false)
    }

    override fun hideLoading() {
        pbMatch.gone()
        rvMatch.visible()
        tvEmpty.gone()
        isLoad = true
    }

    override fun showLoading() {
        pbMatch.visible()
        rvMatch.gone()
        tvEmpty.gone()
        isLoad = false
    }

    override fun showEmptyData() {
        pbMatch.gone()
        rvMatch.gone()
        tvEmpty.visible()
    }

    override fun displayTeamBadgeHome(team: Team, position: Int) {
        match[position].homeBadge = team.strTeamBadge
    }

    override fun displayTeamBadgeAway(team: Team, position: Int) {
        match[position].awayBadge = team.strTeamBadge
    }

    override fun displayMatch(events: List<Event>) {
        val service = FootballApiService.getClient().create(FootballRest::class.java)
        val request = MatchRepositoryImpl(service)
        nextMatchPresenter =
            NextMatchPresenter(
                this,
                request,
                scheduler
            )

        match.clear()

        events.forEachIndexed { i, event ->
            match.add(
                ListMatch(
                    null,
                    event.idEvent,
                    event.strDate,
                    event.strHomeTeam,
                    event.intHomeScore,
                    " ",
                    event.strHomeGoalDetails,
                    event.strHomeLineupDefense,
                    event.strHomeLineupForward,
                    event.strHomeLineupGoalkeeper,
                    event.strHomeLineupMidfield,
                    event.strHomeLineupSubstitutes,
                    event.strAwayTeam,
                    event.intAwayScore,
                    " ",
                    event.strAwayGoalDetails,
                    event.strAwayLineupDefense,
                    event.strAwayLineupForward,
                    event.strAwayLineupGoalkeeper,
                    event.strAwayLineupMidfield,
                    event.strAwayLineupSubstitutes
                )
            )
        }

        var position = 0

        events.forEachIndexed { i, event ->
            nextMatchPresenter.getTeamsBadgeAway(event.idAwayTeam ?: " ", position)
            nextMatchPresenter.getTeamsBadgeHome(event.idHomeTeam ?: " ", position)
            position++
        }

        matchAdapter = MatchAdapter(match, context)

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvMatch.layoutManager = layoutManager
        rvMatch.adapter = matchAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        nextMatchPresenter.onDestroyPresenter()
    }

}


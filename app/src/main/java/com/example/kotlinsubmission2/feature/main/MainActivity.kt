package com.example.kotlinsubmission2.feature.main

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.example.kotlinsubmission2.R
import com.example.kotlinsubmission2.adapter.LeaguesAdapter
import com.example.kotlinsubmission2.feature.favorite.FavoriteActivity
import com.example.kotlinsubmission2.model.League
import com.example.kotlinsubmission2.model.repository.LeagueRepositoryImpl
import com.example.kotlinsubmission2.source.api.FootballApiService
import com.example.kotlinsubmission2.source.api.FootballRest
import com.example.kotlinsubmission2.util.gone
import com.example.kotlinsubmission2.util.visible
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_toolbar_main.*
import org.jetbrains.anko.AnkoLogger

class MainActivity : AppCompatActivity(), AnkoLogger, MainContract.View {

    private lateinit var leaguesAdapter: LeaguesAdapter
    private lateinit var mainPresenter : MainPresenter
    private var leagueLists : MutableList<League> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val service = FootballApiService.getClient().create(FootballRest::class.java)
        val request = LeagueRepositoryImpl(service)
        mainPresenter = MainPresenter(this, request)
        mainPresenter.getLeagueData()

        fabFavorites.setOnClickListener {
            FavoriteActivity.start(this)
        }
    }

    override fun hideLoading() {
        pbLeague.gone()
        rvLeague.visible()
    }

    override fun showLoading() {
        pbLeague.visible()
        rvLeague.gone()
    }

    override fun displayLeague(leagueList: List<League>) {
        Log.v("league", ""+leagueList.size)
        leagueLists.clear()
        leagueLists.addAll(leagueList)

        leaguesAdapter = LeaguesAdapter(
            leagueList,
            applicationContext
        )

        rvLeague.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(this@MainActivity, 2)
            adapter = leaguesAdapter
        }
    }
}

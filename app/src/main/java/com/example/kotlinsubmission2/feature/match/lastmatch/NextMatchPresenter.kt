package com.example.kotlinsubmission2.feature.match.lastmatch

import com.example.kotlinsubmission2.feature.match.MatchContract
import com.example.kotlinsubmission2.model.repository.MatchRepositoryImpl
import com.example.kotlinsubmission2.util.SchedulerProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class NextMatchPresenter(
    private val mView: MatchContract.View,
    private val matchRepositoryImpl: MatchRepositoryImpl,
    private val scheduler: SchedulerProvider
) :
    MatchContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun getMatchData(idLeague: String) {
        mView.showLoading()
        compositeDisposable.add(matchRepositoryImpl.getNextMatch(idLeague)
            .observeOn(scheduler.ui())
            .subscribeOn(scheduler.io())
            .subscribe({
                mView.displayMatch(it.events)
                mView.hideLoading()
            }, {
                mView.hideLoading()
                mView.showEmptyData()
            })
        )
    }

    override fun getTeamsBadgeAway(id: String, position: Int) {
        compositeDisposable.add(matchRepositoryImpl.getTeamsDetail(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe {
                mView.displayTeamBadgeAway(it.teams[0], position)
            })
    }

    override fun getTeamsBadgeHome(id: String, position: Int) {
        compositeDisposable.add(matchRepositoryImpl.getTeamsDetail(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe {
                mView.displayTeamBadgeHome(it.teams[0], position)
            })
    }

    override fun onDestroyPresenter() {
        compositeDisposable.dispose()
    }

}
package com.example.kotlinsubmission2.feature.detail

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import com.example.kotlinsubmission2.R
import com.example.kotlinsubmission2.adapter.PagerAdapter
import com.example.kotlinsubmission2.feature.searchmatches.SearchMatchActivity
import com.example.kotlinsubmission2.model.League
import com.example.kotlinsubmission2.util.setImageUrl
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.startActivity

class DetailActivity : AppCompatActivity() {
    private var isFavorite: Boolean = false

    companion object {
        fun start(
            context: Context,
            league : League?) {
            context.startActivity<LeagueDetailActivity>(
                "league" to league
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val league = intent?.getParcelableExtra<League>("league")

        toolbarDetail.title = league?.strLeague
        setSupportActionBar(toolbarDetail)
        vpMatch.adapter = PagerAdapter(supportFragmentManager, league)
        tlMatch.setupWithViewPager(vpMatch)

        initData(league)
        setScrollingBehavior(true)

        btnMore.setOnClickListener {
            LeagueDetailActivity.start(this@DetailActivity, league)
        }
    }

    private fun setScrollingBehavior(isScroll: Boolean) {
        val params = containerDetail.layoutParams as AppBarLayout.LayoutParams
        params.scrollFlags = if (isScroll)
            AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or
                    AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or
                    AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
        else 0

        containerDetail.layoutParams = params
    }

    private fun initData(league: League?){

        imgLeague.setImageUrl(this, league?.strBadge ?: " ", R.drawable.not_found)

        tvLeagueName.text = league?.strLeague
        tvLeagueDetail.text = league?.strDescriptionEN
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)

        val searchView = menu?.findItem(R.id.actionSearch)?.actionView as SearchView?

        searchView?.queryHint = "Type team to search"

        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                val league = intent?.getParcelableExtra<League>("league")

                SearchMatchActivity.start(this@DetailActivity, query, league)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}
package com.example.kotlinsubmission2.feature.match.nextmatch

import com.example.kotlinsubmission2.feature.match.MatchContract
import com.example.kotlinsubmission2.model.repository.MatchRepositoryImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class PreviousMatchPresenter(private val mView : MatchContract.View, private val matchRepositoryImpl: MatchRepositoryImpl) :
    MatchContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun getMatchData(idLeague: String) {
        mView.showLoading()
        compositeDisposable.add(matchRepositoryImpl.getPreviousMatch(idLeague)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                mView.displayMatch(it.events)
                mView.hideLoading()
            }, {
                mView.hideLoading()
                mView.showEmptyData()
            }))
    }

    override fun getTeamsBadgeAway(id: String, position : Int) {
        compositeDisposable.add(matchRepositoryImpl.getTeamsDetail(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe{
                mView.displayTeamBadgeAway(it.teams[0], position)
            })
    }

    override fun getTeamsBadgeHome(id: String, position: Int) {
        compositeDisposable.add(matchRepositoryImpl.getTeamsDetail(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe{
                mView.displayTeamBadgeHome(it.teams[0], position)
            })
    }

    override fun onDestroyPresenter() {
        compositeDisposable.dispose()
    }

}
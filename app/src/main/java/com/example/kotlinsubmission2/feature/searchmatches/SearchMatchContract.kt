package com.example.kotlinsubmission2.feature.searchmatches

import com.example.kotlinsubmission2.model.Event
import com.example.kotlinsubmission2.model.Team

interface SearchMatchContract {

    interface View{
        fun showLoading()
        fun hideLoading()
        fun showEmpty()
        fun displayMatch(events: List<Event>)
        fun displayTeamBadgeHome(team: Team, position: Int = 0)
        fun displayTeamBadgeAway(team: Team, position: Int = 0)
    }
    interface Presenter{
        fun searchMatch(query: String?)
        fun getTeamsBadgeAway(id:String, position: Int = 0)
        fun getTeamsBadgeHome(id:String, position: Int = 0)
        fun onDestroy()
    }

}
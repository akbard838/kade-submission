package com.example.kotlinsubmission2.feature.favorite

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlinsubmission2.R
import com.example.kotlinsubmission2.adapter.MatchAdapter
import com.example.kotlinsubmission2.model.ListMatch
import com.example.kotlinsubmission2.source.db.database
import kotlinx.android.synthetic.main.activity_search_match.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.startActivity

class FavoriteActivity : AppCompatActivity() {

    private var match: MutableList<ListMatch> = mutableListOf()
    private lateinit var matchAdapter: MatchAdapter

    companion object {
        fun start(
            context: Context) {
            context.startActivity<FavoriteActivity>()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_match)

        toolbarSearch.title = "Favorited Match"
        setSupportActionBar(toolbarSearch)

        matchAdapter = MatchAdapter(
            match,
            this
        )

        showFavorite()

        rvSearch.apply {
            setHasFixedSize(true)
            layoutManager =
                LinearLayoutManager(this@FavoriteActivity, LinearLayoutManager.VERTICAL, false)
            adapter = matchAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        showFavorite()
    }

    private fun showFavorite(){
        match.clear()
        this.database.use {
            val result = select(ListMatch.TABLE_FAVORITE)
            val listMatch = result.parseList(classParser<ListMatch>())
            match.addAll(listMatch)
            matchAdapter.notifyDataSetChanged()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
package com.example.kotlinsubmission2.feature.detail

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kotlinsubmission2.R
import com.example.kotlinsubmission2.model.League
import com.example.kotlinsubmission2.util.setImageUrl
import kotlinx.android.synthetic.main.activity_league_detail.*
import org.jetbrains.anko.startActivity

class LeagueDetailActivity : AppCompatActivity() {

    companion object {
        fun start(
            context: Context,
            league : League?) {
            context.startActivity<LeagueDetailActivity>(
                "league" to league
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_league_detail)

        val league = intent?.getParcelableExtra<League>("league")

        initData(league)

        toolbarLeagueDetail.title = league?.strLeague
        setSupportActionBar(toolbarLeagueDetail)
    }

    private fun initData(league: League?){
        imgLeague.setImageUrl(this, league?.strBadge ?: " ", R.drawable.not_found)

        tvName.text = "${league?.strLeague} (${league?.intFormedYear})"
        tvDetail.text = league?.strDescriptionEN
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}
package com.example.kotlinsubmission2.feature.main

import com.example.kotlinsubmission2.model.repository.LeagueRepositoryImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainPresenter(private val mView : MainContract.View, private val leagueRepositoryImpl: LeagueRepositoryImpl) : MainContract.Presenter{

    private val compositeDisposable = CompositeDisposable()

    override fun getLeagueData() {
        mView.showLoading()
        compositeDisposable.add(leagueRepositoryImpl.getLeagues("soccer")
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe {
                mView.displayLeague(it.countrys)
                mView.hideLoading()
            })
    }
}
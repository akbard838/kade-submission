package com.example.kotlinsubmission2.feature.searchmatches

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlinsubmission2.R
import com.example.kotlinsubmission2.adapter.MatchAdapter
import com.example.kotlinsubmission2.source.api.FootballApiService
import com.example.kotlinsubmission2.source.api.FootballRest
import com.example.kotlinsubmission2.model.Event
import com.example.kotlinsubmission2.model.League
import com.example.kotlinsubmission2.model.ListMatch
import com.example.kotlinsubmission2.model.Team
import com.example.kotlinsubmission2.model.repository.MatchRepositoryImpl
import com.example.kotlinsubmission2.util.gone
import com.example.kotlinsubmission2.util.visible
import kotlinx.android.synthetic.main.activity_search_match.*
import org.jetbrains.anko.startActivity

class SearchMatchActivity : AppCompatActivity(), SearchMatchContract.View {

    private var league : League? = null
    private var query : String? = null
    private var filteredEvents: List<Event> = listOf()
    private lateinit var matchAdapter: MatchAdapter
    lateinit var searchMatchPresenter: SearchMatchContract.Presenter
    private val match: MutableList<ListMatch> = mutableListOf()

    companion object {
        fun start(
            context: Context,
            query : String?,
            league : League?) {
            context.startActivity<SearchMatchActivity>(
                "query" to query,
                "league" to league
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_match)

        toolbarSearch.title = " "
        setSupportActionBar(toolbarSearch)

        league = intent?.getParcelableExtra("league")
        query = intent.getStringExtra("query")
        Log.v("test", query ?: " ")
        val service = FootballApiService.getClient().create(FootballRest::class.java)
        val request = MatchRepositoryImpl(service)
        searchMatchPresenter = SearchMatchPresenter(this, request)
        searchMatchPresenter.searchMatch(query)

    }

    override fun showLoading() {
        pbSearch.visible()
        rvSearch.gone()
        tvMessage.gone()
    }

    override fun hideLoading() {
        pbSearch.gone()
        rvSearch.visible()
        tvMessage.gone()
    }

    override fun showEmpty() {
        pbSearch.gone()
        rvSearch.gone()
        tvMessage.visible()
    }

    override fun displayTeamBadgeHome(team: Team, position: Int) {
        match[position].homeBadge = team.strTeamBadge
    }

    override fun displayTeamBadgeAway(team: Team, position: Int) {
        match[position].awayBadge = team.strTeamBadge
    }

    override fun displayMatch(events: List<Event>) {
        val service = FootballApiService.getClient().create(FootballRest::class.java)
        val request = MatchRepositoryImpl(service)
        searchMatchPresenter = SearchMatchPresenter(this, request)

        var position = 0

        filteredEvents =
            events.filter { it.strSport == "Soccer" && it.idLeague == league?.idLeague}

        if (filteredEvents.isNotEmpty()) {
            match.clear()

            filteredEvents.forEachIndexed { _, event ->
                match.add(
                    ListMatch(
                        null,
                        event.idEvent,
                        event.strDate,
                        event.strHomeTeam,
                        event.intHomeScore,
                        " ",
                        event.strHomeGoalDetails ?: "-",
                        event.strHomeLineupDefense ?: "-",
                        event.strHomeLineupForward ?: "-",
                        event.strHomeLineupGoalkeeper ?: "-",
                        event.strHomeLineupMidfield ?: "-",
                        event.strHomeLineupSubstitutes ?: "-",
                        event.strAwayTeam,
                        event.intAwayScore,
                        " ",
                        event.strAwayGoalDetails ?: "-",
                        event.strAwayLineupDefense ?: "-",
                        event.strAwayLineupForward ?: "-",
                        event.strAwayLineupGoalkeeper ?: "-",
                        event.strAwayLineupMidfield ?: "-",
                        event.strAwayLineupSubstitutes ?: "-"
                    )
                )
                searchMatchPresenter.getTeamsBadgeAway(event.idAwayTeam ?: " ", position)
                searchMatchPresenter.getTeamsBadgeHome(event.idHomeTeam ?: " ", position)
                position++
            }


            matchAdapter = MatchAdapter(
                match,
                this
            )

            rvSearch.apply {
                layoutManager =
                    LinearLayoutManager(this@SearchMatchActivity, LinearLayoutManager.VERTICAL, false)
                adapter = matchAdapter
            }
        } else {
            tvMessage.visible()
            pbSearch.gone()
            rvSearch.gone()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)

        val searchView = menu?.findItem(R.id.actionSearch)?.actionView as SearchView?

        searchView?.queryHint = "Type team to search"

        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                searchMatchPresenter.searchMatch(newText)
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}

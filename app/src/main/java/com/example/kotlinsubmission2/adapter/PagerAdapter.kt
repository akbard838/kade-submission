package com.example.kotlinsubmission2.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.kotlinsubmission2.feature.match.lastmatch.NextMatchFragment
import com.example.kotlinsubmission2.feature.match.nextmatch.PreviousMatchFragment
import com.example.kotlinsubmission2.model.League

class PagerAdapter(fm: FragmentManager, league : League?): FragmentPagerAdapter(fm){

    private val pages : List<Fragment> = listOf(
        PreviousMatchFragment.newInstance(league),
        NextMatchFragment.newInstance(league)
    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Previous Match"
            else -> "Next Match"
        }
    }
}
package com.example.kotlinsubmission2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlinsubmission2.R
import com.example.kotlinsubmission2.feature.detail.MatchDetailActivity
import com.example.kotlinsubmission2.model.ListMatch
import com.example.kotlinsubmission2.util.setImageUrl
import kotlinx.android.synthetic.main.layout_item_match.view.*


class MatchAdapter(val eventList:MutableList<ListMatch> = mutableListOf(), val context:Context?): RecyclerView.Adapter<MatchAdapter.MatchViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchViewHolder {
        return MatchViewHolder(LayoutInflater.from(context).inflate(R.layout.item_match, parent, false))
    }

    override fun getItemCount(): Int = eventList.size


    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bind(eventList[position])
    }

    inner class MatchViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView) {
        fun bind(match:ListMatch){
            with(itemView){
                if(match.homeScore == null) {
                    tvDate.setTextColor(itemView.context.
                        resources.getColor(R.color.colorGreenTwo))
                } else {
                    tvDate.setTextColor(itemView.context.
                        resources.getColor(R.color.colorDenimBlue))
                }

                imgHome.setImageUrl(context, match.homeBadge ?: " ", pbHome, R.drawable.not_found)
                imgAway.setImageUrl(context, match.awayBadge ?: " ", pbAway, R.drawable.not_found)

                tvDate.text = match.date
                tvNameHome.text = match.homeName
                tvScoreHome.text = match.homeScore
                tvNameAway.text = match.awayName
                tvScoreAway.text = match.awayScore

                setOnClickListener {
                    MatchDetailActivity.start(context, match)
                }

            }
        }
    }

}
package com.example.kotlinsubmission2.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlinsubmission2.R
import com.example.kotlinsubmission2.feature.detail.DetailActivity
import com.example.kotlinsubmission2.model.League
import com.example.kotlinsubmission2.util.setImageUrl
import kotlinx.android.synthetic.main.item_league.view.*
import org.jetbrains.anko.intentFor

class LeaguesAdapter(val leagueList:List<League>, val context:Context): RecyclerView.Adapter<LeaguesAdapter.LeagueViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeagueViewHolder {
        return LeagueViewHolder(LayoutInflater.from(context).inflate(R.layout.item_league, parent, false))
    }

    override fun getItemCount(): Int = leagueList.size


    override fun onBindViewHolder(holder: LeagueViewHolder, position: Int) {
        holder.bind(leagueList[position])
    }

    inner class LeagueViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView) {
        fun bind(league:League){
            with(itemView) {
                tvLeagueName.text = league.strLeague

                imgLeague.setImageUrl(context, league.strBadge ?: " ", R.drawable.not_found )

                itemView.setOnClickListener {
                    itemView.context.startActivity(
                        context.intentFor<DetailActivity>("league" to league).addFlags(
                            Intent.FLAG_ACTIVITY_NEW_TASK
                        )
                    )
                }

            }
        }
    }
}
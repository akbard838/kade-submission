package com.example.kotlinsubmission2.source.api

import com.example.kotlinsubmission2.model.FootballMatch
import com.example.kotlinsubmission2.model.Leagues
import com.example.kotlinsubmission2.model.SearchResult
import com.example.kotlinsubmission2.model.Teams
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface FootballRest {

    @GET("search_all_leagues.php")
    fun getLeague(@Query("s") s :String) : Flowable<Leagues>

    @GET("eventspastleague.php")
    fun getPreviousMatch(@Query("id") id:String) : Flowable<FootballMatch>

    @GET("eventsnextleague.php")
    fun getNextMatch(@Query("id") id:String) : Flowable<FootballMatch>

    @GET("searchevents.php")
    fun searchMatches(@Query("e") query: String?) : Flowable<SearchResult>

    @GET("lookupevent.php")
    fun getEventById(@Query("id") id:String) : Flowable<FootballMatch>

    @GET("lookupteam.php")
    fun getTeam(@Query("id") id:String) : Flowable<Teams>
}
package com.example.kotlinsubmission2.source.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.kotlinsubmission2.model.ListMatch
import org.jetbrains.anko.db.*

class MyDatabaseOpenHelper(context: Context) : ManagedSQLiteOpenHelper(context, "FavoriteMatch.db", null, 1) {
    companion object {
        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if (instance == null) {
                instance = MyDatabaseOpenHelper(ctx.applicationContext)
            }
            return instance as MyDatabaseOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(
            ListMatch.TABLE_FAVORITE, true,
            ListMatch.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            ListMatch.MATCH_ID to TEXT + UNIQUE,
            ListMatch.MATCH_DATE to TEXT,
            ListMatch.HOME_NAME to TEXT,
            ListMatch.HOME_SCORE to TEXT,
            ListMatch.HOME_BADGE to TEXT,
            ListMatch.HOME_GOALS to TEXT,
            ListMatch.HOME_DEFENSE to TEXT,
            ListMatch.HOME_FORWARD to TEXT,
            ListMatch.HOME_GOALKEEPER to TEXT,
            ListMatch.HOME_MIDFIELD to TEXT,
            ListMatch.HOME_SUBS to TEXT,
            ListMatch.AWAY_NAME to TEXT,
            ListMatch.AWAY_SCORE to TEXT,
            ListMatch.AWAY_BADGE to TEXT,
            ListMatch.AWAY_GOALS to TEXT,
            ListMatch.AWAY_DEFENSE to TEXT,
            ListMatch.AWAY_FORWARD to TEXT,
            ListMatch.AWAY_GOALKEEPER to TEXT,
            ListMatch.AWAY_MIDFIELD to TEXT,
            ListMatch.AWAY_SUBS to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(ListMatch.
            TABLE_FAVORITE, true)
    }
}

val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)
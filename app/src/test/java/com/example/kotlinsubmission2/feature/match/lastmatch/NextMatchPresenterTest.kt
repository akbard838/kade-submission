package com.example.kotlinsubmission2.feature.match.lastmatch

import com.example.kotlinsubmission2.feature.match.MatchContract
import com.example.kotlinsubmission2.model.Event
import com.example.kotlinsubmission2.model.FootballMatch
import com.example.kotlinsubmission2.model.repository.MatchRepositoryImpl
import com.example.kotlinsubmission2.util.SchedulerProvider
import com.example.kotlinsubmission2.util.TestSchedulerProvider
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class NextMatchPresenterTest {
    @Mock
    lateinit var mView: MatchContract.View

    @Mock
    lateinit var matchRepositoryImpl: MatchRepositoryImpl

    lateinit var mPresenter: NextMatchPresenter

    lateinit var scheduler: SchedulerProvider

    lateinit var match : FootballMatch

    lateinit var footballMatch: Flowable<FootballMatch>

    private val event = mutableListOf<Event>()

    @Before
    fun setUp() {
        scheduler = TestSchedulerProvider()
        MockitoAnnotations.initMocks(this)
        match = FootballMatch(event)
        footballMatch = Flowable.just(match)
        mPresenter = NextMatchPresenter(mView, matchRepositoryImpl, scheduler)
        Mockito.`when`(matchRepositoryImpl.getNextMatch("4328")).thenReturn(footballMatch)
    }

    @Test
    fun getMatchData() {
        mPresenter.getMatchData()
        Mockito.verify(mView).showLoading()
        Mockito.verify(mView).displayMatch(event)
        Mockito.verify(mView).hideLoading()
    }
}